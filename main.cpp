#include <iostream>
#include "precompiler/code_gen/code_generator.h"
#include "precompiler/steps/generation_step.h"
#include "precompiler/processor_manager.h"
#include "precompiler/log.h"
#include "precompiler/steps/shader_node_generation_step.h"
#include "precompiler/steps/property_generation_step.h"
#include <cxxopts.hpp>
#include <fstream>

mv_compiler::generation_step*
create_generation_step(const std::string& project_dir, const std::string& engine_include_dir)
{
    auto res = new mv_compiler::generation_step();

    res->add_generation_attribute(
            mv_compiler::generation_attribute{"mv_register_property_renderer", "mv_register_property_renderer",
                                              "registering field renderer"});
    res->add_generation_attribute(
            mv_compiler::generation_attribute{"mv_register_window", "mv_register_window", "registering window"});

    res->add_generation_attribute(
            mv_compiler::generation_attribute{"mv_register_property_gizmo_renderer", "mv_register_property_gizmo_renderer",
                                              "registering gizmo renderer", {{"PropertyViewportRenderer"}}});


    res->set_project_dir(project_dir);

    res->set_engine_include_dir(engine_include_dir);

    return res;
}

mv_compiler::shader_node_generation_step*
create_shader_node_generation_step(const std::string& project_dir, const std::string& engine_include_dir)
{
    auto res = new mv_compiler::shader_node_generation_step("mv_register_shader_node", "mv_register_shader_node");

    res->set_project_dir(project_dir);

    res->set_engine_include_dir(engine_include_dir);

    return res;
}

mv_compiler::property_generation_step*
create_property_generation_step(const std::string& project_dir, const std::string& engine_include_dir)
{
    auto res = new mv_compiler::property_generation_step("mv_register_property");

    res->set_project_dir(project_dir);

    res->set_engine_include_dir(engine_include_dir);

    return res;
}

void
register_steps(mv_compiler::processor& processor, const std::string& project_dir, const std::string& engine_include_dir)
{
    processor.add_step(create_generation_step(project_dir, engine_include_dir));
    processor.add_step(create_shader_node_generation_step(project_dir, engine_include_dir));
    processor.add_step(create_property_generation_step(project_dir, engine_include_dir));
}

std::vector<std::string> read_files_from_file(const std::string& path)
{
    std::vector<std::string> res;

    std::ifstream ifs{path, std::ios::in};

    if (!ifs.is_open())
    {
        std::cerr << "Failed to open file for read " << path << std::endl;
    }

    std::string input;
    ifs >> input;

    ifs.close();

    size_t pos = 0;
    while ((pos = input.find(',')) != std::string::npos)
    {
        std::string token = input.substr(0, pos);
        if (token.length() > 0)
        {
            res.push_back(std::move(token));
        }
        input.erase(0, pos + 1);
    }

    return res;
}

int main(int argc, char* argv[])
{
    mv_log::create_logger();

    cxxopts::Options options("mv_precompiler", "Precompiler for MV engine.");
    options.add_options()
            ("include_dirs", "Include directories of project", cxxopts::value<std::string>())
            ("files", "Files to process", cxxopts::value<std::string>())
            ("single_file", "Single file to process", cxxopts::value<std::string>())
            ("output_directory", "Output directory", cxxopts::value<std::string>())
            ("v", "Verbose output", cxxopts::value<bool>()->default_value("false"))
            ("editor_include_directory", "Editor include directory", cxxopts::value<std::string>())
            ("engine_include_directory", "Engine include directory", cxxopts::value<std::string>())
            ("disable_cache", "Disables caching", cxxopts::value<bool>()->default_value("false"));

    cxxopts::ParseResult res;

    try
    {
        res = options.parse(argc, argv);
    } catch (std::exception& e)
    {
        mv_log::fatal("Failed to parse input params.");
    }

    if (!res.count("files") && !res.count("single_file"))
    {
        mv_log::fatal("Missing files param.");
    }

    if (!res.count("output_directory"))
    {
        mv_log::fatal("Missing output directory.");
    }

    if (!res.count("editor_include_directory"))
    {
        mv_log::fatal("Missing editor include directory.");
    }

    if (!res.count("engine_include_directory"))
    {
        mv_log::fatal("Missing engine include directory.");
    }

    std::vector<std::string> files{};
    std::vector<std::string> include_dirs{};
    std::string output_dir;
    output_dir = res["output_directory"].as<std::string>();

    if (res.count("single_file"))
    {
        files.emplace_back(res["single_file"].as<std::string>());
    } else
    {
        files = read_files_from_file(res["files"].as<std::string>());
    }

    if (res["include_dirs"].count())
    {
        include_dirs = read_files_from_file(res["include_dirs"].as<std::string>());
    }

    bool verbose = false;
    bool enable_cache = true;

    if (res.count("disable_cache"))
    {
        enable_cache = false;
    }

    if (res.count("v"))
    {
        verbose = true;
    }

    auto& codeGenerator = mv_compiler::code_generator::instance();
    codeGenerator.load_templates();

    mv_threading::processor_manager manager{};

    if(enable_cache)
        manager.cache->load();

    manager.dispatch(files, include_dirs, output_dir, [&](mv_compiler::processor& pc)
    {
        register_steps(pc, res["editor_include_directory"].as<std::string>(),
                       res["engine_include_directory"].as<std::string>());
        pc.set_verbose(verbose);
    });

    if(enable_cache)
        manager.cache->save();

    return 0;
}
