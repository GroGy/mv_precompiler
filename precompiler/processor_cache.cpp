//
// Created by Matty on 02.04.2023.
//

#include <iostream>
#include <shared_mutex>
#include <fstream>
#include "processor_cache.h"
#include "sha256.h"
#include "log.h"

namespace mv_compiler
{
    bool processor_cache::is_in_cache(const std::string & path, const std::string& hash) const
    {
        std::shared_lock<decltype(mutex)> lk(mutex);

        auto res = cache.find(path);

        if(res == cache.end()) return false;

        bool matches = res->second == hash;

        return matches;
    }

    void processor_cache::add_to_cache(const std::string & path, const std::string& hash)
    {
        std::unique_lock<decltype(mutex)> lk(mutex);
        cache[path] = hash;
    }

    void processor_cache::load(const std::string& cache_name)
    {
        std::ifstream ifs{cache_name, std::ios::in};

        if(!ifs.is_open()) {
            mv_log::error("Failed to load cache: {}", cache_name);
            return;
        }

        std::unique_lock<decltype(mutex)> lk(mutex);

        std::string line;
        while(getline(ifs, line)){
            auto separator_index = line.find('|');
            if(separator_index == std::string::npos) {
                mv_log::error("Found line in cache with missing separator: {}", line);
                continue;
            }

            auto path = line.substr(0, separator_index);
            auto hash = line.substr(separator_index + 1, line.length());

            cache[path] = hash;
        }

        ifs.close();
    }

    void processor_cache::save(const std::string& cache_name)
    {
        std::ofstream ofs{cache_name, std::ios::out};

        if(!ofs.is_open()) {
            mv_log::error("Failed to save cache: {}", cache_name);
            return;
        }

        std::shared_lock<decltype(mutex)> lk(mutex);

        for(auto && v : cache) {
            std::string line;

            line += v.first;
            line += "|";
            line += v.second;
            line += "\n";

            ofs << line;
        }

        ofs.close();

    }

    std::string get_file_hash(const std::string& path)
    {
        std::ifstream ifs{path, std::ios::in};

        if(!ifs.is_open()) {
            mv_log::error("Failed to open file for hash calc: {}", path);
            return "";
        }

        std::string src_str;

        ifs >> src_str;

        ifs.close();

        auto hex_str = sha256(src_str);

        return hex_str;
    }
}