//
// Created by Matty on 2022-11-06.
//

#ifndef MV_PRECOMPILER_PROCESSOR_STEP_H
#define MV_PRECOMPILER_PROCESSOR_STEP_H

#include <string_view>
#include "processor_state.h"

namespace mv_compiler {
    class processor_step {
    public:
        virtual ~processor_step();

        virtual void apply(processor_state& state,const std::string & output_directory) = 0;

        [[nodiscard]]
        virtual std::string_view get_name() const = 0;
    protected:

    private:

    };
}


#endif //MV_PRECOMPILER_PROCESSOR_STEP_H
