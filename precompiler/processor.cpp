#include "processor.h"
#include <iostream>
#include "cppast/diagnostic_logger.hpp"
#include "cppast/compile_config.hpp"
#include "cppast/libclang_parser.hpp"
#include "processor_state.h"
#include "log.h"

namespace mv_compiler {
    void processor::process(const std::string & path, const std::vector<std::string> & include_dirs, const std::string & output_directory,const std::shared_ptr<processor_cache> & cache) {
        if(steps.empty()) {
            mv_log::fatal("No processor steps added!");
        }

        try {
            auto config = create_config(include_dirs);

            cppast::stderr_diagnostic_logger logger;

            auto hsh = get_file_hash(path);

            if(cache->is_in_cache(path, hsh)) {
                if(verbose) {
                    mv_log::log("(Cache) Skipping file: {}", path);
                }
                return;
            }

            auto res = open_file(config, logger, path,true);
            if (!res) {
                mv_log::fatal("Error when opening file: {}", path);
            }

            if(verbose) {
                mv_log::log("Processing file: {}", path);
            }

            process_data(res,output_directory);

            cache->add_to_cache(path, hsh);
        } catch (const cppast::libclang_error& ex)
        {
            mv_log::error("CLang error: {}", path);
        }
        catch (std::exception & e) {
            mv_log::fatal("Fatal error: {} ({})", e.what(), path);
        }
    }

    std::unique_ptr<cppast::cpp_file>
    processor::open_file(const cppast::libclang_compile_config& config,
                         const cppast::diagnostic_logger&       logger,
                         const std::string& path, bool fatal_error) {
        cppast::cpp_entity_index idx;
        cppast::libclang_parser parser(type_safe::ref(logger));
        auto file = parser.parse(idx, path, config);
        if (fatal_error && parser.error())
            return nullptr;
        return file;
    }

    void processor::set_verbose(bool val) {
        verbose = val;
    }

    void processor::process_data(const std::unique_ptr<cppast::cpp_file> &file, const std::string & output_directory) {
        processor_state state{};

        state.file = &file;
        state.verbose = verbose;

        for(auto && step : steps)
            step->apply(state,output_directory);
    }

    processor::~processor() {
        for(auto & step : steps) {
            delete step;
        }
    }

    void processor::add_step(processor_step* step)
    {
        steps.emplace_back(step);
    }

    cppast::libclang_compile_config processor::create_config(const std::vector<std::string> & include_dirs)
    {
        cppast::libclang_compile_config config;

        config.write_preprocessed(false);

        config.fast_preprocessing(false);

        config.remove_comments_in_macro(false);

        for(auto && inc_dir : include_dirs)
            config.add_include_dir(inc_dir);

        cppast::compile_flags flags;
        //if (options.count("gnu_extensions"))
        flags |= cppast::compile_flag::ms_extensions;
        //if (options.count("msvc_compatibility"))
        //    flags |= cppast::compile_flag::ms_compatibility;

        config.set_flags(cppast::cpp_standard::cpp_20, flags);

        return config;
    }
}
