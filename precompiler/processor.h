

#include "cppast/cpp_file.hpp"
#include "processor_step.h"
#include "processor_cache.h"

namespace mv_compiler
{
    class processor
    {
    public:
        virtual ~processor();

        void process(const std::string& path, const std::vector<std::string>& include_dirs,
                     const std::string& output_directory, const std::shared_ptr<processor_cache>& cache);

        void set_verbose(bool val);

        void add_step(processor_step* step);

    private:

        std::unique_ptr<cppast::cpp_file> open_file(const cppast::libclang_compile_config& config,
                                                    const cppast::diagnostic_logger& logger,
                                                    const std::string& filename, bool fatal_error);

        void process_data(const std::unique_ptr<cppast::cpp_file>& file, const std::string& output_directory);

        cppast::libclang_compile_config create_config(const std::vector<std::string>& include_dirs);

    private: // Fields

        bool verbose = false;

        std::vector<processor_step*> steps;
    };
}