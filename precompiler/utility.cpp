//
// Created by Matty on 01.04.2023.
//

#include "utility.h"

std::string mv_compiler::to_upper(const std::string& str)
{
    std::string res = str;

    for(auto & c : res) {
        c = std::toupper(c);
    }

    return res;
}

std::string mv_compiler::to_lower(const std::string& str)
{
    std::string res = str;

    for(auto & c : res) {
        c = std::tolower(c);
    }

    return res;
}

bool mv_compiler::is_hex_notation(const std::string& str)
{
    return str.compare(0, 2, "0x") == 0
           && str.size() > 2
           && str.find_first_not_of("0123456789abcdefABCDEF", 2) == std::string::npos;
}
