//
// Created by Matty on 02.04.2023.
//

#ifndef MV_PRECOMPILER_LOG_H
#define MV_PRECOMPILER_LOG_H

#include <iostream>
#include "spdlog/spdlog.h"
#include "spdlog/sinks/rotating_file_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"

namespace mv_log {
    extern std::shared_ptr<spdlog::logger> & get();

    inline void create_logger() {
        spdlog::flush_every(std::chrono::seconds(1));
        auto cout_console_sink= std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
        auto cerr_console_sink= std::make_shared<spdlog::sinks::stderr_color_sink_mt>();
        cerr_console_sink->set_level(spdlog::level::err);
        cout_console_sink->set_level(spdlog::level::info);

        try
        {
            // Create a file rotating logger with 5mb size max and 3 rotated files
            auto max_size = 1048576 * 5;
            auto max_files = 3;

            auto rot_file_sink = std::make_shared<spdlog::sinks::rotating_file_sink_mt>("log.txt", max_size, max_files);

            auto sinks = std::initializer_list<std::shared_ptr<spdlog::sinks::sink>>{cout_console_sink, cerr_console_sink, rot_file_sink};

            get() = std::make_shared<spdlog::logger>("Precompiler", sinks);

        }
        catch (const spdlog::spdlog_ex &ex)
        {
            std::cerr << "Failed to init rotating sink. Failed: " << ex.what() << std::endl;

            auto sinks = std::initializer_list<std::shared_ptr<spdlog::sinks::sink>>{cout_console_sink, cerr_console_sink};

            get() = std::make_shared<spdlog::logger>("Precompiler", sinks);
        }
    }

    template<typename... Args>
    inline void log(spdlog::format_string_t<Args...> fmt, Args &&... args){
        get()->info(fmt, std::forward<Args>(args)...);
    }

    template<typename... Args>
    inline void error(spdlog::format_string_t<Args...> fmt, Args &&... args){
        get()->error(fmt, std::forward<Args>(args)...);
    }

    template<typename... Args>
    inline void fatal(spdlog::format_string_t<Args...> fmt, Args &&... args){
        get()->error(fmt, std::forward<Args>(args)...);
        get()->flush();
        exit(-1);
    }
}


#endif //MV_PRECOMPILER_LOG_H
