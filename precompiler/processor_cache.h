//
// Created by Matty on 02.04.2023.
//

#ifndef MV_PRECOMPILER_PROCESSOR_CACHE_H
#define MV_PRECOMPILER_PROCESSOR_CACHE_H


#include <string>
#include <unordered_map>
#include <shared_mutex>

namespace mv_compiler
{
    class processor_cache
    {
    public:

        bool is_in_cache(const std::string & path, const std::string & hash) const;

        void add_to_cache(const std::string & path, const std::string & hash);

        void load(const std::string & cache_name = "file_cache.mv_cache");

        void save(const std::string & cache_name = "file_cache.mv_cache");

    private:

        mutable std::shared_mutex mutex;

        // File path -> hash
        std::unordered_map<std::string,std::string> cache;

    };

    std::string get_file_hash(const std::string & path);
}


#endif //MV_PRECOMPILER_PROCESSOR_CACHE_H
