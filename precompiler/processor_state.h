//
// Created by Matty on 2022-11-06.
//

#ifndef MV_PRECOMPILER_PROCESSOR_STATE_H
#define MV_PRECOMPILER_PROCESSOR_STATE_H

#include <filesystem>
#include "cppast/cpp_file.hpp"

namespace mv_compiler {
    class processor_state {
    public:
        bool cancel = false;
        bool verbose = false;
        std::filesystem::path file_hashes;
        const std::unique_ptr<cppast::cpp_file>* file;
    };
}


#endif //MV_PRECOMPILER_PROCESSOR_STATE_H
