//
// Created by Matty on 2022-11-11.
//

#ifndef MV_PRECOMPILER_CODE_GENERATOR_H
#define MV_PRECOMPILER_CODE_GENERATOR_H

#include <unordered_map>
#include <optional>
#include "nlohmann/json.hpp"

namespace mv_compiler {
    class code_generator {
    public:

        static code_generator &instance();

    public:

        void load_templates();

        void generate_file(const std::string & class_name, const std::string & name, const nlohmann::json & data, const std::string & output_dir);

        void remove_file_if_exists(const std::string & class_name, const std::string & output_dir);

        void createTemplate(std::string_view name, std::string_view path, std::string_view src_path);

    private: // Helper functions

        std::optional<std::string> loadTemplate(std::string_view path);

        void write_to_file(const std::string& path, const std::string & value);

    private: // Internal fields

        std::unordered_map<std::string, std::pair<std::string,std::string>> loadedTemplates;
    };
}


#endif //MV_PRECOMPILER_CODE_GENERATOR_H
