//
// Created by Matty on 2022-11-11.
//

#ifndef MV_PRECOMPILER_CODE_FILE_TEMPLATE_H
#define MV_PRECOMPILER_CODE_FILE_TEMPLATE_H

#include <string_view>

namespace mv_compiler {
    static constexpr std::string_view code_template =
            "";
}

#endif //MV_PRECOMPILER_CODE_FILE_TEMPLATE_H
