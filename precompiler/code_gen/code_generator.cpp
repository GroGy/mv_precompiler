//
// Created by Matty on 2022-11-11.
//

#include <fstream>
#include <iostream>
#include <filesystem>
#include "code_generator.h"
#include "inja/inja.hpp"
#include "../log.h"

namespace mv_compiler
{
    code_generator& code_generator::instance()
    {
        static code_generator singleton;
        return singleton;
    }

    void code_generator::load_templates()
    {
        createTemplate("mv_register_property", "register_property_template.jinja",
                       "register_property_src_template.jinja");

        createTemplate("mv_register_property_renderer", "register_field_renderer_template.jinja",
                       "register_field_renderer_src_template.jinja");

        createTemplate("mv_register_window", "register_window_template.jinja",
                       "register_window_src_template.jinja");

        createTemplate("mv_register_property_gizmo_renderer", "register_property_gizmo_renderer_template.jinja",
                       "register_property_gizmo_renderer_src_template.jinja");

        createTemplate("mv_register_shader_node", "register_shader_node_template.jinja",
                       "register_shader_node_src_template.jinja");

        //createTemplate("register_node","../register_node_template.jinja");
    }

    std::optional<std::string> code_generator::loadTemplate(std::string_view path)
    {
        std::ifstream file{path.data()};

        if (!file.is_open())
            return {};

        std::stringstream ss;

        ss << file.rdbuf();

        file.close();

        return std::make_optional(ss.str());
    }

    void
    code_generator::generate_file(const std::string& class_name, const std::string& name, const nlohmann::json& data,
                                  const std::string& output_dir)
    {
        auto foundRes = loadedTemplates.find(name);
        if (foundRes == loadedTemplates.end())
        {
            mv_log::fatal("Missing code template: {}", name);
        }

        std::string headerFileName = "generated." + class_name + ".h";
        std::string srcFileName = "generated." + class_name + ".cpp";

        std::string res;
        std::string res_src;
        try
        {
            res = inja::render(foundRes->second.first, data);
            auto srcData = data;
            srcData["generated_header"] = headerFileName;
            res_src = inja::render(foundRes->second.second, srcData);
        }
        catch (std::exception& e)
        {
            mv_log::fatal("Failed to render header: {} with error:\n\t{}", name,e.what());
            exit(-1);
        }

        std::filesystem::path output = output_dir;

        std::filesystem::create_directories(output);

        {
            auto headerPath = output;
            headerPath /= headerFileName;

            write_to_file(headerPath.string(), res);
        }

        {
            auto srcPath = output;
            srcPath /= srcFileName;

            write_to_file(srcPath.string(), res_src);
        }

    }

#ifdef NDEBUG
#define MAKE_PATH(filename) filename
#else
#define MAKE_PATH(filename) std::string("../") + filename
#endif

    void code_generator::createTemplate(std::string_view name, std::string_view path, std::string_view src_path)
    {
        auto header_template = loadTemplate(MAKE_PATH(path.data()));
        if (!header_template.has_value())
        {
            mv_log::fatal("Failed to load header template: {}", MAKE_PATH(src_path.data()));
        }


        auto src_template = loadTemplate(MAKE_PATH(src_path.data()));
        if (!src_template.has_value())
        {
            mv_log::fatal("Failed to load source template: {}", MAKE_PATH(src_path.data()));
        }

        auto& templates = loadedTemplates[name.data()];

        templates.first = header_template.value();
        templates.second = src_template.value();
    }

    void code_generator::write_to_file(const std::string& path, const std::string& value)
    {
        std::ofstream stream{path, std::ios::out};

        if(!stream.is_open()) {
            mv_log::fatal("Failed to open file for writing: {}", path);
            exit(-1);
        }

        stream << value;

        stream.close();
    }

    void code_generator::remove_file_if_exists(const std::string& class_name, const std::string & output_dir)
    {
        std::filesystem::path output = output_dir;

        std::string headerFileName = "generated." + class_name + ".h";
        std::string srcFileName = "generated." + class_name + ".cpp";

        auto headerPath = output;
        headerPath /= headerFileName;

        auto srcPath = output;
        srcPath /= srcFileName;

        std::filesystem::remove(headerPath);
        std::filesystem::remove(srcPath);
    }
}
