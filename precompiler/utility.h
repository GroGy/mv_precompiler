//
// Created by Matty on 01.04.2023.
//

#ifndef MV_PRECOMPILER_UTILITY_H
#define MV_PRECOMPILER_UTILITY_H


#include <string>

namespace mv_compiler {
    std::string to_upper(const std::string& str);
    std::string to_lower(const std::string& str);
    bool is_hex_notation(const std::string& str);
}

#endif //MV_PRECOMPILER_UTILITY_H
