//
// Created by Matty on 02.05.2023.
//

#include "shader_node_generation_step.h"
#include "cppast/cpp_token.hpp"
#include "cppast/visitor.hpp"
#include "../log.h"
#include "../utility.h"
#include "../code_gen/code_generator.h"

namespace mv_compiler
{

    void shader_node_generation_step::apply(processor_state& state, const std::string& output_directory)
    {
        context ctx;
        cppast::visit(**state.file, [&](const cppast::cpp_entity& e, cppast::visitor_info info)
        {
            const auto kind = e.kind();
            if (e.kind() == cppast::cpp_entity_kind::file_t || cppast::is_templated(e)
                || cppast::is_friended(e))
            {
                file_path = e.name();
                file_name = file_path.stem().string();
                return true;
            } else if (info.event == cppast::visitor_info::container_entity_exit)
            {
                // we have visited all children of a container
                if (e.kind() != cppast::cpp_entity_kind::enum_t)
                {
                    if (!namespace_scopes.empty())
                        namespace_scopes.pop_back();
                }
                return true;
            } else
            {
                if (e.kind() == cppast::cpp_entity_kind::namespace_t)
                {
                    namespace_scopes.emplace_back(e.name());
                }

                if (e.kind() == cppast::cpp_entity_kind::class_template_t)
                {
                    namespace_scopes.emplace_back(e.name());
                }

                if (e.kind() == cppast::cpp_entity_kind::class_t)
                {
                    process_class(e, info, state, output_directory, ctx);
                    namespace_scopes.emplace_back(e.name());
                }

                return true;
            }
        });

        if (ctx.generate_code)
        {
            auto fn = file_name;
            for(auto & c : fn)
                if(c == ' ') c = '_';
            ctx.data["file_name_upper"] = to_upper(fn);
            ctx.data["file_path_relative_to_editor"] = get_class_path_relative();
            ctx.data["file_path_relative_to_engine"] = get_class_path_to_engine_relative();

            mv_compiler::code_generator::instance().generate_file(file_name,code_template_name, ctx.data, output_directory);
        }
    }

    std::string_view shader_node_generation_step::get_name() const
    {
        return "shader node generation step";
    }

    void shader_node_generation_step::process_class(const cppast::cpp_entity& e, cppast::visitor_info info,
                                                    processor_state& state, const std::string& output_dir, context& ctx)
    {
        class_name = e.name();

        nlohmann::json classInfo;
        process_class_attributes(e, info, state, output_dir, classInfo);

        if(!classInfo.empty()) {
            ctx.generate_code = true;
            ctx.data["classes"].emplace_back(classInfo);
        }
    }

    void shader_node_generation_step::process_class_attributes(const cppast::cpp_entity& e, cppast::visitor_info info,
                                                               processor_state& state, const std::string& output_dir,
                                                               nlohmann::json& classInfo)
    {
        if (e.attributes().empty()) return;

        for (auto& att: e.attributes())
        {
            if(att.name() != attribute_name) continue;

            mv_log::log("Found attribute on shader node {} ({})", class_name, att.name());
            if (!att.arguments().has_value())
            {
                mv_log::fatal("Missing arguments on attribute {} on class {}!", att.name(), class_name);
            }

            auto & args = att.arguments().value();

            uint32_t argCounter = 0;
            bool showInPopup = true;
            std::string nodeName;

            for(auto && argument : args) {
                if(argument.kind ==cppast::cpp_token_kind::punctuation) continue;
                argCounter++;

                if(argCounter == 1) { // First argument is always name
                    nodeName = (argument.spelling);
                } else if(argCounter == 2) {
                    if(argument.spelling == "HideInPopup")
                        showInPopup = false;
                } else {
                    mv_log::fatal("Way too many arguments on attribute {} on class {}! ({})", att.name(), class_name, argument.spelling);
                }
            }

            classInfo["name_with_namespace"] = get_class_with_namespace();
            classInfo["class_name"] = class_name;
            classInfo["shader_node_show_in_popup"] = showInPopup ? "true" : "false";
            classInfo["shader_node_name"] = "{"+ nodeName;
        }
    }

    shader_node_generation_step::shader_node_generation_step(std::string_view attribute_name_in,
                                                             std::string_view code_template_name_in)
    {
        attribute_name = attribute_name_in;
        code_template_name = code_template_name_in;
    }
}