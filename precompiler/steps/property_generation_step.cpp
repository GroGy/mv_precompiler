//
// Created by Matty on 22.08.2023.
//

#include "property_generation_step.h"
#include "../utility.h"
#include "../code_gen/code_generator.h"
#include "cppast/visitor.hpp"
#include "../log.h"

namespace mv_compiler
{
    void mv_compiler::property_generation_step::apply(processor_state& state, const std::string& output_directory)
    {
        context ctx;
        cppast::visit(**state.file, [&](const cppast::cpp_entity& e, cppast::visitor_info info)
        {
            const auto kind = e.kind();
            if (e.kind() == cppast::cpp_entity_kind::file_t || cppast::is_templated(e)
                || cppast::is_friended(e))
            {
                file_path = e.name();
                file_name = file_path.stem().string();
                return true;
            } else if (info.event == cppast::visitor_info::container_entity_exit)
            {
                // we have visited all children of a container
                if (e.kind() != cppast::cpp_entity_kind::enum_t)
                {
                    if (!namespace_scopes.empty())
                        namespace_scopes.pop_back();
                }
                return true;
            } else
            {
                if (e.kind() == cppast::cpp_entity_kind::namespace_t)
                {
                    namespace_scopes.emplace_back(e.name());
                }

                if (e.kind() == cppast::cpp_entity_kind::class_template_t)
                {
                    namespace_scopes.emplace_back(e.name());
                }

                if (e.kind() == cppast::cpp_entity_kind::class_t)
                {
                    process_class(e, info, state, output_directory, ctx);
                    namespace_scopes.emplace_back(e.name());
                }

                if(e.kind() == cppast::cpp_entity_kind::member_variable_t) {
                    process_class_field(e, info, state, ctx);
                }

                return true;
            }
        });

        if (ctx.generate_code)
        {
            if(!ctx.class_data_set) {
                mv_log::fatal("No valid class found!");
            }

            auto fn = file_name;
            for(auto & c : fn)
                if(c == ' ') c = '_';
            ctx.data["file_name_upper"] = to_upper(fn);
            ctx.data["file_path_relative_to_editor"] = get_class_path_relative();
            ctx.data["file_path_relative_to_engine"] = get_class_path_to_engine_relative();
            ctx.data["is_animatable"] = ctx.is_animatable;

            mv_compiler::code_generator::instance().generate_file(file_name,code_template_name, ctx.data, output_directory);
        } else {
            mv_compiler::code_generator::instance().remove_file_if_exists(code_template_name, output_directory);
        }
    }

    std::string_view mv_compiler::property_generation_step::get_name() const
    {
        return "property generation step";
    }

    void
    mv_compiler::property_generation_step::process_class(const cppast::cpp_entity& e, cppast::visitor_info info,
                                                         processor_state& state, const std::string& output_dir,
                                                         mv_compiler::property_generation_step::context& ctx)
    {
        class_name = e.name();

        nlohmann::json classInfo;
        process_class_attributes(e, info, state, output_dir, classInfo);

        if(!classInfo.empty()) {
            if(ctx.class_data_set) {
                mv_log::fatal("Properties dont support multiple property classes in single file!");
            }
            ctx.generate_code = true;
            ctx.class_data_set = true;
            ctx.data["class_name_with_namespace"] = classInfo["name_with_namespace"];
            ctx.data["class_name"] = classInfo["class_name"];
        }
    }

    void
    mv_compiler::property_generation_step::process_class_attributes(const cppast::cpp_entity& e,
                                                                    cppast::visitor_info info,
                                                                    processor_state& state,
                                                                    const std::string& output_dirn,
                                                                    nlohmann::json& classInfo)
    {
        if (e.attributes().empty()) return;

        for (auto& att: e.attributes())
        {
            if(att.name() != sc_RegisterPropertyAttributeName) continue;

            mv_log::log("Found attribute on property {} ({})", class_name, att.name());
            if (!att.arguments().has_value())
            {
                mv_log::fatal("Missing arguments on attribute {} on class {}!", att.name(), class_name);
            }

            classInfo["name_with_namespace"] = get_class_with_namespace();
            classInfo["class_name"] = class_name;
        }
    }

    void property_generation_step::process_field_attributes(const cppast::cpp_entity& e, cppast::visitor_info info,
                                                            processor_state& state, nlohmann::json& data, bool & animable)
    {
        if(e.attributes().empty()) return;

        for(auto& att : e.attributes()) {
            auto name = att.name();

            if(name == sc_AnimatableFieldAttributeName.data()) {
                auto & args = att.arguments().value();

                mv_log::log("Found animatable attribute {} on field {}::{}", name, class_name, e.name());

                uint32_t argCounter = 0;
                std::string fieldIDStr;
                std::string fieldName;

                enum IdWritingState {
                    None,
                    Eq,
                    Value
                };
                IdWritingState writingId = None;


                for(auto && argument : args) {
                    if(argument.spelling == ",") continue;

                    argCounter++;

                    if(argCounter == 1) { // First argument is always name
                        fieldName = (argument.spelling);
                        continue;
                    }

                    if(writingId == None && argument.spelling == "ID") {
                        writingId = Eq;
                        continue;
                    }

                    if(writingId == Eq) {
                        if(argument.spelling != "=") {
                            mv_log::fatal("Invalid attribute {} usage on {}::{}", sc_AnimatableFieldAttributeName, class_name, e.name());
                        }
                        writingId = Value;
                        continue;
                    }

                    if(writingId == Value) {
                        fieldIDStr = argument.spelling;
                        writingId = None;
                        continue;
                    }
                }

                if(fieldIDStr.empty() || writingId != None)
                    mv_log::fatal("Missing ID field {} attribute on {}::{}. Add it like this [[{}({},ID = 0xNNNNNNNN)]], where ID is your choice. (must be valid hex 64bit number)", sc_AnimatableFieldAttributeName, class_name, e.name(), sc_AnimatableFieldAttributeName,fieldName);

                if(!is_hex_notation(fieldIDStr)) {
                    mv_log::fatal("Invalid ID field in attribute {} on {}::{}. Add it like this [[{}({},ID = 0xNNNNNNNN)]], where ID is your choice. (must be valid hex 64bit number) Passed ID:{}", sc_AnimatableFieldAttributeName, class_name, e.name(), sc_AnimatableFieldAttributeName,fieldName, fieldIDStr);
                }

                animable = true;
                data["id"] = fieldIDStr;
                data["field_name"] = e.name();
                data["name"] = fieldName;
                continue;
            }


            mv_log::log("Found unprocessed attribute {} on field {}::{}", name, class_name, e.name());
        }

    }

    void property_generation_step::process_class_field(const cppast::cpp_entity& e, cppast::visitor_info info,
                                                       processor_state& state,
                                                       mv_compiler::property_generation_step::context& ctx)
    {
        auto field = e.name();

        nlohmann::json fieldInfo;
        bool animable = false;
        process_field_attributes(e, info, state, fieldInfo, animable);

        if(!fieldInfo.empty()) {
            if(animable) {
                ctx.is_animatable = true;
            }

            ctx.generate_code = true;
            ctx.data["animable_fields"].emplace_back(fieldInfo);
        }
    }

    property_generation_step::property_generation_step(std::string_view code_template_name_in)
    {
        code_template_name = code_template_name_in;
    }

}