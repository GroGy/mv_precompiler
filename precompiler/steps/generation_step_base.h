//
// Created by Matty on 02.05.2023.
//

#ifndef MV_PRECOMPILER_GENERATION_STEP_BASE_H
#define MV_PRECOMPILER_GENERATION_STEP_BASE_H

#include "../processor_step.h"

namespace mv_compiler {
    class generation_step_base : public processor_step
    {
    public:

        void set_project_dir(const std::string & project_dir);

        void set_engine_include_dir(const std::string & project_dir);

    protected:

        std::string get_class_with_namespace();

        /// Gets relative path to class path (relative to project path)
        std::string get_class_path_relative();

        std::string get_class_path_to_engine_relative();

        std::string remove_double_quotes(const std::string & input);

    protected:

        std::vector<std::string> namespace_scopes;

        std::filesystem::path file_path;

        std::filesystem::path project_path;

        std::filesystem::path engine_include_path;

        std::string class_name;
    };
}


#endif //MV_PRECOMPILER_GENERATION_STEP_BASE_H
