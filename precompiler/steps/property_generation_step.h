//
// Created by Matty on 22.08.2023.
//

#ifndef MV_PRECOMPILER_PROPERTY_GENERATION_STEP_H
#define MV_PRECOMPILER_PROPERTY_GENERATION_STEP_H

#include "generation_step_base.h"
#include "nlohmann/json.hpp"

namespace mv_compiler
{
    static constexpr std::string_view sc_AnimatableFieldAttributeName = "mv_animatable";
    static constexpr std::string_view sc_RegisterPropertyAttributeName = "mv_register_property";
    static constexpr std::string_view sc_SerializeFieldAttributeName = "mv_serialize";

    struct property_generation_context {
        nlohmann::json data;
        bool generate_code = false;
        bool is_animatable = false;
        bool class_data_set = false;
    };

    class property_generation_step : public generation_step_base {
        using context = property_generation_context;

    public:
        property_generation_step(std::string_view code_template_name);

        void apply(processor_state &state,const std::string & output_directory) override;

        [[nodiscard]]
        std::string_view get_name() const override;

    private:

        void process_class(const cppast::cpp_entity &entity, cppast::visitor_info info, processor_state & state, const std::string & output_dir, context & ctx);

        void process_class_attributes(const cppast::cpp_entity &e, cppast::visitor_info info,processor_state & state, const std::string & output_dirn,nlohmann::json& classInfo);

        void process_field_attributes(const cppast::cpp_entity &e, cppast::visitor_info info,processor_state & state, nlohmann::json& data, bool & animable);

        void process_class_field(const cppast::cpp_entity& entity, cppast::visitor_info info, processor_state& state, context & ctx);

    private:

        std::string code_template_name;

        std::string file_name;

    };
}


#endif //MV_PRECOMPILER_PROPERTY_GENERATION_STEP_H
