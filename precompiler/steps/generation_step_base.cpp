//
// Created by Matty on 02.05.2023.
//

#include <cassert>
#include "generation_step_base.h"

namespace mv_compiler
{
    std::string generation_step_base::get_class_with_namespace()
    {
        std::string res;
        for(auto && ns : namespace_scopes) {
            res += ns;
            res += "::";
        }

        res += class_name;

        return res;
    }

    std::string generation_step_base::get_class_path_relative()
    {
        auto res = std::filesystem::relative(file_path,project_path).string();

        for(auto & c : res) {
            if(c == '\\') c = '/';
        }

        return res;
    }

    void generation_step_base::set_project_dir(const std::string& project_dir)
    {
        project_path = project_dir;
    }

    void generation_step_base::set_engine_include_dir(const std::string& dir)
    {
        engine_include_path = dir;
    }

    std::string generation_step_base::get_class_path_to_engine_relative()
    {
        auto res = std::filesystem::relative(file_path,engine_include_path).string();

        for(auto & c : res) {
            if(c == '\\') c = '/';
        }

        return res;
    }

    std::string generation_step_base::remove_double_quotes(const std::string& input)
    {
        assert(input.size() > 2);
        return input.substr(1, input.size() - 2);
    }
}