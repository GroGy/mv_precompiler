//
// Created by Matty on 2022-11-07.
//

#ifndef MV_PRECOMPILER_GENERATION_STEP_H
#define MV_PRECOMPILER_GENERATION_STEP_H

#include "generation_step_base.h"

namespace mv_compiler {
    struct attribute_base_class_requirement {
        std::string class_name;
    };

    struct generation_attribute {
        generation_attribute(std::string  attributeName, std::string  templateName,
                             std::string  printOnVerbose, std::vector<attribute_base_class_requirement> && reqBaseClasses = {});

        std::string attribute_name;
        std::string template_name;
        std::string print_on_verbose;
        std::vector<attribute_base_class_requirement> possible_req_base_classes;
    };

    class generation_step : public generation_step_base {
    public:
        void apply(processor_state &state,const std::string & output_directory) override;

        [[nodiscard]]
        std::string_view get_name() const override;

        void add_generation_attribute(generation_attribute && att);

    private:
        void process_class(const cppast::cpp_entity &entity, cppast::visitor_info info, processor_state & state, const std::string & output_dir);
        void process_class_attributes(const cppast::cpp_entity &e, cppast::visitor_info info,processor_state & state, const std::string & output_dir);


        std::vector<generation_attribute> registered_attributes;

    };
}


#endif //MV_PRECOMPILER_GENERATION_STEP_H
