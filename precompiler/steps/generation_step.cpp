//
// Created by Matty on 2022-11-07.
//

#include <iostream>
#include <utility>
#include "generation_step.h"
#include "cppast/visitor.hpp"
#include "nlohmann/json.hpp"
#include "../code_gen/code_generator.h"
#include "../utility.h"
#include "../log.h"

namespace mv_compiler {
    void generation_step::apply(processor_state &state, const std::string & output_directory) {
        cppast::visit(**state.file, [&](const cppast::cpp_entity& e, cppast::visitor_info info) {
            const auto kind = e.kind();
            if (e.kind() == cppast::cpp_entity_kind::file_t || cppast::is_templated(e)
                || cppast::is_friended(e)) {
                file_path = e.name();
                return true;
            }
            else if (info.event == cppast::visitor_info::container_entity_exit)
            {
                // we have visited all children of a container
                if(e.kind() != cppast::cpp_entity_kind::enum_t) {
                    if(!namespace_scopes.empty())
                        namespace_scopes.pop_back();
                }
                return true;
            }
            else
            {
                if(e.kind() == cppast::cpp_entity_kind::namespace_t) {
                    namespace_scopes.emplace_back(e.name());
                }

                if(e.kind() == cppast::cpp_entity_kind::class_t) {
                    process_class(e,info,state, output_directory);
                    namespace_scopes.emplace_back(e.name());
                }

                return true;
            }
        });
    }

    std::string_view generation_step::get_name() const {
        return "generation_step";
    }

    void generation_step::process_class(const cppast::cpp_entity &e, cppast::visitor_info info,processor_state & state,  const std::string & output_dir) {
        class_name = e.name();

        process_class_attributes(e, info, state, output_dir);
    }

    void generation_step::add_generation_attribute(generation_attribute&& att)
    {
        registered_attributes.push_back(std::move(att));
    }

    void generation_step::process_class_attributes(const cppast::cpp_entity& e, cppast::visitor_info info,
                                                   processor_state& state, const std::string & output_dir)
    {
        if(e.attributes().empty()) return;

        for(auto && att : registered_attributes) {
            bool found = false;
            for(const auto & v : e.attributes()) {
                if(v.kind() == cppast::cpp_attribute_kind::unknown && v.name() == att.attribute_name) {
                    found = true;
                }
            }

            if(!found) continue;

            auto & c = (cppast::cpp_class&) e;

            if(!att.possible_req_base_classes.empty()) {
                if(c.bases().empty()) {
                    mv_log::fatal("Class {} attribute {} is missing one of required parent classes!", class_name, e.attributes()[0].name());
                }

                bool foundP = false;

                for(auto && bs : c.bases()) {
                    if(foundP) continue;

                    const auto& name = bs.name();

                    for(auto && posPar : att.possible_req_base_classes) {
                        if(name.find(posPar.class_name) != std::string::npos) {
                            foundP = true;
                            continue;
                        }
                    }


                }

                if(!foundP) {
                    mv_log::fatal("Class {} attribute {} is missing one of required parent classes!", class_name, e.attributes()[0].name());
                }
            }

            if(state.verbose) std::cout << att.print_on_verbose << "\n";

            nlohmann::json generate_data;
            generate_data["class_name"] = e.name();
            generate_data["class_name_upper"] = to_upper(e.name());
            generate_data["class_name_with_namespace"] = get_class_with_namespace();
            generate_data["class_path_relative_to_editor"] = get_class_path_relative();
            generate_data["class_path_relative_to_engine"] = get_class_path_to_engine_relative();

            mv_compiler::code_generator::instance().generate_file(e.name(),att.template_name, generate_data, output_dir);
        }
    }

    generation_attribute::generation_attribute(std::string attributeName, std::string templateName,
                                               std::string printOnVerbose, std::vector<attribute_base_class_requirement> && reqBaseClasses) : attribute_name(std::move(attributeName)),
                                                                                    template_name(std::move(templateName)),
                                                                                    print_on_verbose(std::move(printOnVerbose)),
                                                                                    possible_req_base_classes(std::move(reqBaseClasses))
    {}
}