//
// Created by Matty on 02.05.2023.
//

#ifndef MV_PRECOMPILER_SHADER_NODE_GENERATION_STEP_H
#define MV_PRECOMPILER_SHADER_NODE_GENERATION_STEP_H

#include "generation_step_base.h"
#include "nlohmann/json.hpp"

namespace mv_compiler
{
    struct shader_node_generation_context {
        nlohmann::json data;
        bool generate_code = false;
    };

    class shader_node_generation_step : public generation_step_base {
        using context = shader_node_generation_context;

    public:
        shader_node_generation_step(std::string_view attribute_name, std::string_view code_template_name);

        void apply(processor_state &state,const std::string & output_directory) override;

        [[nodiscard]]
        std::string_view get_name() const override;

    private:

        void process_class(const cppast::cpp_entity &entity, cppast::visitor_info info, processor_state & state, const std::string & output_dir, context & ctx);

        void process_class_attributes(const cppast::cpp_entity &e, cppast::visitor_info info,processor_state & state, const std::string & output_dirn,nlohmann::json& classInfo);

    private:

        std::string attribute_name;

        std::string code_template_name;

        std::string file_name;

    };
}


#endif //MV_PRECOMPILER_SHADER_NODE_GENERATION_STEP_H
