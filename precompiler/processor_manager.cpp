//
// Created by Matty on 02.04.2023.
//

#include <queue>
#include <iostream>
#include "processor_manager.h"

namespace mv_threading
{
    void processor_manager::dispatch(const std::vector<std::string>& files,
                                     const std::vector<std::string>& include_dirs,
                                     const std::string& output_directory, const processor_init_fn & init_fn)
    {

        for(uint32_t i = 0; i < std::thread::hardware_concurrency(); i++) {
            workers.emplace_back(std::make_shared<processor_worker>(include_dirs, output_directory,init_fn,cache));
        }

        for(auto && w : workers) {
            w->init();
        }

        std::queue<std::string> file_queue;

        for(auto & f : files) {
            if(!f.ends_with(".h")) continue;
            file_queue.push(f);
        }

        while(!file_queue.empty()) {
            auto f = file_queue.front();
            bool dispatched = false;

            for(auto && w : workers) {
                if(w->is_free()) {
                    w->dispatch_task(f);
                    file_queue.pop();
                    dispatched = true;
                    break;
                }
            }

            if(!dispatched)
                std::this_thread::sleep_for(std::chrono::milliseconds(100));

            std::cerr << std::flush;

        }

        for(auto && w : workers) {
            w->stop();
        }

        workers.clear();
    }

    processor_manager::processor_manager()
    {
        cache = std::make_shared<mv_compiler::processor_cache>();
    }


    void processor_worker::init()
    {
        init_fn(processor_w);

        thread = std::thread([&](){
           thread_main();
        });
    }

    void processor_worker::thread_main()
    {
        std::unique_lock<decltype(mutex)> lk{mutex};

        while(run) {
            cv.wait(lk, [&]()-> bool {
                return !run || !current_file.empty();
            });

            if(!run)
                break;

            processor_w.process(current_file, include_dirs, output_directory, cache);

            current_file.clear();
        }

        finished = true;
    }

    void processor_worker::stop()
    {
        run = false;
        cv.notify_all();

        while(!finished)
            std::this_thread::sleep_for(std::chrono::milliseconds(10));

        thread.join();
    }

    processor_worker::processor_worker(const std::vector<std::string>& include_dirsi,
                                       const std::string& output_directoryi, const processor_init_fn & init_fni,const std::shared_ptr<mv_compiler::processor_cache> & cache)
        : include_dirs(include_dirsi), output_directory(output_directoryi), init_fn(init_fni), cache(cache)
    {

    }

    void processor_worker::dispatch_task(const std::string& task)
    {
        current_file = task;
        cv.notify_all();
    }
}
