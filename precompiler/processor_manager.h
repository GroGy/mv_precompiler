//
// Created by Matty on 02.04.2023.
//

#ifndef MV_PRECOMPILER_PROCESSOR_MANAGER_H
#define MV_PRECOMPILER_PROCESSOR_MANAGER_H

#include <string>
#include <vector>
#include <thread>
#include <mutex>
#include "processor.h"
#include "processor_cache.h"

namespace mv_threading
{
    using processor_init_fn = std::function<void(mv_compiler::processor &)>;

    class processor_worker
    {
    public:
        processor_worker() = default;

        processor_worker(const std::vector<std::string>& include_dirs, const std::string& output_directory, const processor_init_fn & init_fn, const std::shared_ptr<mv_compiler::processor_cache> & cache);

        void init();

        void stop();

        void dispatch_task(const std::string & task);

        [[nodiscard]]
        bool is_free() const { return current_file.empty(); };
    private:

        void thread_main();

    private:

        mv_compiler::processor processor_w;

        mutable std::mutex mutex{};

        std::condition_variable cv{};

        std::thread thread;

        std::string current_file{};

        bool run = true;

        bool finished = false;

        std::vector<std::string> include_dirs;

        std::string output_directory;

        processor_init_fn init_fn;

        std::shared_ptr<mv_compiler::processor_cache> cache;
    };

    class processor_manager
    {
    public:
        processor_manager();

        void dispatch(const std::vector<std::string>& files, const std::vector<std::string>& include_dirs,
                      const std::string& output_directory, const processor_init_fn & init_fn);

        std::shared_ptr<mv_compiler::processor_cache> cache;

    private:

        std::vector<std::shared_ptr<processor_worker>> workers;


    };
}


#endif //MV_PRECOMPILER_PROCESSOR_MANAGER_H
