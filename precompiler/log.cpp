//
// Created by Matty on 02.04.2023.
//

#include "log.h"

namespace mv_log {
    static std::shared_ptr<spdlog::logger> g_Logger;
    std::shared_ptr<spdlog::logger> & get()
    {
        return g_Logger;
    }
}